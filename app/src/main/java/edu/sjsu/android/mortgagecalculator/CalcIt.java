package edu.sjsu.android.mortgagecalculator;

import java.lang.Math;
public class CalcIt
{
    public static float calcMortgage(float inputValue, float intVal, int numYears, boolean taxes)
    {
        float payment = 0;
        float p = inputValue;
        float j = (float)(intVal)/1200;
        int n = numYears*12;
        float t = 0;

        if (j==0)
        {
            if (taxes==true)
            {
                t = (float) (0.001*p);
                payment = (p/n)+t;
                return payment;

            }
            else
            {
               payment = (p/n);
               return payment;
            }
        }
        else
        {

            if (taxes==true)
            {
                t = (float)(0.001*p);
                payment = (float) (j/(1-Math.pow(1+j,(-1*n))))*p + t;
                return payment;
            }
            else
            {
                payment = (float) (j/(1-Math.pow(1+j,(-1*n))))*p;
                return payment;
            }
        }
    }
}
