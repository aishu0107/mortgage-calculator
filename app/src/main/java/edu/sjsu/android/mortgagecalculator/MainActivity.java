package edu.sjsu.android.mortgagecalculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import android.os.Bundle;

public class MainActivity extends Activity
{
    private EditText text;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (EditText)findViewById(R.id.editText);
    }

    public void onClick (View view)
    {
        int years = 0;
        boolean taxes = true;

        switch (view.getId())
        {
            case R.id.button:
                RadioButton fifteen = (RadioButton)findViewById(R.id.fifteen);
                RadioButton twenty = (RadioButton)findViewById(R.id.twenty);
                RadioButton thirty = (RadioButton)findViewById(R.id.thirty);
                SeekBar interest = (SeekBar)findViewById(R.id.seekBar);
                CheckBox check = (CheckBox)findViewById(R.id.checkbox);
                TextView display = (TextView)findViewById(R.id.display);
                TextView rate = (TextView)findViewById(R.id.textView4);
                if (text.getText().length()==0 || text.getText().toString().equals("."))
                {
                    Toast.makeText(this,"Please enter a valid number", Toast.LENGTH_LONG).show();
                    return;
                }

                float inputValue = Float.parseFloat(text.getText().toString());
                int seekVal = interest.getProgress();
                float seek = ((float)(interest.getProgress()))/10;
                rate.setText("Yours: "+Float.toString(seek)+" %");

                if (fifteen.isChecked())
                {

                    years = 15;
                }
                else if (twenty.isChecked())
                {

                    years = 20;
                }
                else
                {

                    years = 30;
                }

               if (!check.isChecked())
               {
                   taxes = false;
               }

                display.setText("Monthly Payment: $" + String.valueOf(CalcIt.calcMortgage(inputValue, seek, years, taxes)));

                break;

        }
    }

}